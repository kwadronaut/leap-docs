---
title: Welcome to the LEAP documentation!
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<!-- markdownlint-restore -->

This is the documentation site for the [LEAP Project](https://leap.se).

As of today, LEAP offers building blocks for provisioning a secure VPN service.
As an example, RiseupVPN is deployed used the LEAP VPN Platform, and the
clients are built from the leap-android and leap-desktop codebase.

Here you can find references to technical documentation and tutorials for different parts of the project.

This site is a living index for all the moving parts that need coordination - more detailed
documentation for each project can usually be found within each repository.


## Overview

{{< columns >}}

### Architecture

General overview of the different components used in LEAP.

<--->

### Circumvention

Circumvention techniques used by our clients.

<--->

{{< /columns >}}


## Services

{{< columns >}}

### Menshen

Menshen is the load balancer that helps with gateway and bridge selection.

<--->

### Orchestration

Float is an opinionated way to deploy all the server-side components.

<--->

### API Reference

Documentation for the server-side API.

{{< /columns >}}

## Clients

{{< columns >}}

### Android

Everything related to the android clients.

<--->

### Desktop

Desktop clients.


<--->

### Branding

How to brand your own clients.

{{< /columns >}}

## Extra

{{< columns >}}

### i18n 

How to contribute and maintain i18n for apps and public sites.

<--->

### Contributing

Quick pointers to contribute to any of the sub-projects.

<--->

{{< /columns >}}
